package afrique.digital.signature;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button buttonSign;
    private ImageView imgSignature;
    private String base64SignaturePng = null;

    public static final int CAPTURE_SIGNATURE_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imgSignature = (ImageView) findViewById(R.id.imgSignature);

        buttonSign = (Button) findViewById(R.id.buttonSign);
        buttonSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view)
            {
                Intent intent = new Intent(MainActivity.this, CaptureSignatureActivity.class);
                startActivityForResult(intent, CAPTURE_SIGNATURE_ACTIVITY);
            }
        });
    }

    /**
     * Called when an activity launched by this activity exits.
     * @param requestCode The integer request code originally supplied to
     * startActivityForResult(), allowing you to identify who this result came from.
     * @param resultCode The integer result code returned by the child activity
     * through its setResult().
     * @param data An Intent, which can return result data to the caller
     * (various data can be attached to Intent "extras").
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Bundle extras;

        switch (requestCode)
        {
            case CAPTURE_SIGNATURE_ACTIVITY:
                if (RESULT_OK == resultCode)
                {
                    // Gets the Base64 encoded PNG signature graphic.
                    extras = data.getExtras();
                    base64SignaturePng = extras.getString(CaptureSignatureActivity.BASE64_SIGNATURE_KEY);
                    displaySignature(base64SignaturePng);
                }
                break;
        }
    }

    /**
     * Displays the specified graphic in the imange view next to the Sign button.
     * @param base64Png A Base64 encoded PNG image.
     */
    private void displaySignature(String base64Png)
    {
        if (base64Png != null)
        {
            byte[] signPngBytes = Base64.decode(base64Png, Base64.DEFAULT);
            Bitmap signBitmap = BitmapFactory.decodeByteArray(signPngBytes, 0, signPngBytes.length);
            if (signBitmap != null)
            {
                imgSignature.setImageBitmap(signBitmap);
            }
            else
            {
                // Clears the image view.
                imgSignature.setImageDrawable(null);
            }
        }
        else
        {
            // Clears the image view.
            imgSignature.setImageDrawable(null);
        }
    }
}
